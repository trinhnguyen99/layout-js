var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    res.render('app/admin/index', {title: 'Dashboard', sub_title: 'Dashboard', title_2: "v1"});
});

router.get('/admin/general', function(req, res, next) {
    res.render('app/admin/general', {title: 'General Form Elements', sub_title: 'General Form', title_2: ''});
});

router.get('/admin/login', function(req, res, next) {
    res.render('app/admin/login', {title: 'Log in', key:'login'});
});

router.get('/admin/register', function(req, res, next) {
    res.render('app/admin/register', {title: 'Registration Page',key:'register'});
});


router.get('/admin/simple', function(req, res, next) {
    res.render('app/admin/simple', {title: 'Simple Tables', sub_title: 'Simple Tables',title_2: ''});
});
module.exports = router;
