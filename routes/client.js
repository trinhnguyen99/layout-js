const express = require('express');

const router = express.Router();

router.get('/', (req, res) => {
    res.render('app/client/index', {key_photo: 'home-bg.jpg'});
});
router.get('/about', (req, res) => {
    res.render('app/client/about', {key_photo: 'about-bg.jpg'});
});

router.get('/contact', (req, res) => {
    res.render('app/client/contact',{key_photo: 'contact-bg.jpg'});
});

router.get('/post', (req, res) => {
    res.render('app/client/post',{key_photo: 'post-bg.jpg'});
});

module.exports = router;
